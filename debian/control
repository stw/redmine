Source: redmine
Section: web
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders:  Jérémy Lal <kapouer@melix.org>,
            Lucas Kanashiro <kanashiro@debian.org>,
            Marc Dequènes (Duck) <Duck@DuckCorp.org>
Build-Depends: dbconfig-common,
               debhelper (>= 11),
               ruby | ruby-interpreter,
               ruby-actionpack-action-caching,
               ruby-actionpack-xml-parser,
               ruby-bundler,
               ruby-coderay (>= 1.0.6),
               ruby-csv,
               ruby-i18n (>= 0.7~),
               ruby-jquery-rails (>= 4.0.5),
               ruby-mail (>= 2.7.1~),
               ruby-mimemagic,
               ruby-mime-types (>= 1.25),
               ruby-mini-mime,
               ruby-net-ldap (>= 0.16~),
               ruby-nokogiri (>= 1.10.0~),
               ruby-openid,
               ruby-rack (>= 1.4.5~),
               ruby-rack-openid,
               ruby-rails (>= 2:5.2.2~),
               ruby-rails-observers,
               ruby-rbpdf (>= 1.19~),
               ruby-redcarpet (>= 3.4.0~),
               ruby-request-store,
               ruby-rmagick (>= 2.14.0~),
               ruby-roadie (>= 3.2.2~),
               ruby-roadie-rails (>= 1.3.0~),
               ruby-rouge (>= 3.7.0~)
Build-Depends-Indep: po-debconf
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/ruby-team/redmine
Vcs-Git: https://salsa.debian.org/ruby-team/redmine.git
Homepage: https://www.redmine.org

Package: redmine
Architecture: all
Pre-Depends: debconf
Depends: dbconfig-common,
         redmine-sqlite | redmine-mysql | redmine-pgsql,
         ruby | ruby-interpreter,
         ruby-actionpack-action-caching,
         ruby-actionpack-xml-parser,
         ruby-bundler,
         ruby-coderay (>= 1.0.6),
         ruby-csv,
         ruby-i18n (>= 0.7~),
         ruby-jquery-rails (>= 4.0.5),
         ruby-mail (>= 2.7.1~),
         ruby-mimemagic,
         ruby-mime-types (>= 1.25),
         ruby-mini-mime,
         ruby-net-ldap (>= 0.16~),
         ruby-nokogiri (>= 1.10.0~),
         ruby-openid,
         ruby-rack (>= 1.4.5~),
         ruby-rack-test (>= 0.7.0~),
         ruby-rack-openid,
         ruby-rails (>= 2:5.2.2~),
         ruby-rails-dom-testing (>= 1.0.6-2~),
         ruby-rails-observers,
         ruby-rbpdf (>= 1.19~),
         ruby-redcarpet (>= 3.4.0~),
         ruby-request-store,
         ruby-rmagick (>= 2.14.0~),
         ruby-roadie (>= 3.2.2~),
         ruby-roadie-rails (>= 1.3.0~),
         ruby-rouge (>= 3.7.0~),
         libjs-jquery,
         libjs-jquery-ui,
         libjs-raphael,
         ${misc:Depends}
Breaks: redmine-plugin-botsfilter (<=1.02-2),
        redmine-plugin-recaptcha (<=0.1.0+git20121018)
Conflicts: redmine-plugin-pretend (<= 0.0.2+git20130821-4),
           redmine-plugin-recaptcha (<= 0.1.0+git20121018-3),
           redmine-plugin-local-avatars (<= 1.0.1-1)
Recommends: passenger
Suggests: bzr,
          cvs,
          darcs,
          git,
          mercurial,
          ruby-fcgi,
          subversion
Description: flexible project management web application
 Redmine is a flexible project management web application. Written using Ruby
 on Rails framework, it is cross-platform and cross-database.
 .
 Dependencies for database support are provided by these metapackages:
 redmine-mysql, redmine-pgsql, redmine-sqlite.
 .
 Features
   * Multiple projects support
   * Flexible role based access control
   * Flexible issue tracking system
   * Gantt chart and calendar
   * News, documents & files management
   * Feeds & email notifications
   * Per project wiki
   * Per project forums
   * Time tracking
   * Custom fields for issues, time-entries, projects and users
   * SCM integration (SVN, CVS, Git, Mercurial, Bazaar and Darcs)
   * Issue creation via email
   * Multiple LDAP authentication support
   * User self-registration support
   * Multilanguage support

Package: redmine-mysql
Architecture: all
Depends: dbconfig-mysql | dbconfig-no-thanks,
         default-mysql-client | virtual-mysql-client,
         ruby-mysql2 (>= 0.5.0~),
         ${misc:Depends}
Recommends: redmine (= ${source:Version})
Suggests: default-mysql-server | virtual-mysql-server
Description: metapackage providing MySQL dependencies for Redmine
 This package only provides MySQL dependencies for Redmine, a
 flexible project management web application. Install this
 one if you want to use a MySQL database with Redmine.
 .
 Redmine package should be installed along with this package.

Package: redmine-pgsql
Architecture: all
Depends: dbconfig-pgsql | dbconfig-no-thanks,
         postgresql-client,
         ruby-pg (>= 1.1~),
         ${misc:Depends}
Recommends: redmine (= ${source:Version})
Suggests: postgresql
Description: metapackage providing PostgreSQL dependencies for Redmine
 This package provides PostgreSQL dependencies for Redmine, a
 flexible project management web application. Install this
 one if you want to use a PostgreSQL database with Redmine.
 .
 Redmine package should be installed along with this package.

Package: redmine-sqlite
Architecture: all
Depends: dbconfig-sqlite3 | dbconfig-no-thanks,
         ruby-sqlite3 (>= 1.3.12~),
         sqlite3,
         ${misc:Depends}
Recommends: redmine (= ${source:Version})
Description: metapackage providing sqlite dependencies for Redmine
 This package provides sqlite dependencies for Redmine, a
 flexible project management web application. Install this
 one if you want to use a sqlite database with Redmine.
 .
 Redmine package should be installed along with this package.
